import { Component, OnInit,Input, Output , EventEmitter} from '@angular/core';
import { Data } from 'src/app/json';


@Component({
  selector: 'app-blog-card',
  templateUrl: './blog-card.component.html',
  styleUrls: ['./blog-card.component.scss']
})
export class BlogCardComponent implements OnInit {


  @Input() sendData : any;
  @Output() openModal = new EventEmitter()

  title : any;
  Description:any;
  thumbnail:any;


  constructor() { }


  ngOnInit(): void {

    // console.log(this.sendData);
  
    this.thumbnail=this.sendData.thumbnail;
  }

  card(type:any ){

    this.openModal.emit(type);
  }


}
