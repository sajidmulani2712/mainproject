import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.scss']
})
export class PdfComponent implements OnInit {

  @Input() pdfContent:any;

  constructor() { }

  ngOnInit(): void {
  }

}
