import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {

@Input() videoContent:any;

  constructor() { }

  ngOnInit(): void {
  }

  trackTime(myVideo :any){
this.videoContent.progress = Math.floor ( (100 * myVideo.currentTime )/myVideo.duration) ;
  }

 
 onloaddata(myVideo: any){
        myVideo.currentTime = (this.videoContent.progress/100) * myVideo.duration;
      }

    }