
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-image-container',
  templateUrl: './image-container.component.html',
  styleUrls: ['./image-container.component.scss']
})
export class ImageContainerComponent implements OnInit {

  @Input() allImage:any;
  @Input() type:any;

  isMarked : any;

  // hover:any = -1;
  
  constructor() { }

  ngOnInit(): void {
 // console.log(this.type);
    this.isMarked=this.type.bookmark;
  }

  bookmarkToggle( e:any){
  //  console.log('bookmark press');
   e.stopPropagation();
   
  }


  unMark(){
    console.log(this.type.bookmark);
    this.isMarked = ! this.isMarked;
    this.type.bookmark = this.isMarked;
    console.log(this.type.bookmark);
  }

}
