export class Modal {
  id: number;
  type: string;
  title: string;
  description: string;
  source: string;
  thumbnail: string;
  content: string;
  category: string;
  tags: Array<string>;
  bookmark: boolean;
  status: string;
  progress:number;
  duration:any;

  constructor(data?: any) {
    this.id = data && data.id !== '' ? data.id : '';
    this.type = data && data.type !== '' ? data.type : '';
    this.title = data && data.title !== 0 ? data.title : '';
    this.description = data && data.description !== 0 ? data.description : '';
    this.source = data && data.source !== 0 ? data.source : '';
    this.thumbnail = data && data.thumbnail !== 0 ? data.thumbnail : '';
    this.content = data && data.content !== 0 ? data.content : '';
    this.thumbnail = data && data.thumbnail !== 0 ? data.thumbnail : '';
    this.category = data && data.category !== 0 ? data.category : '';
    this.tags = data && data.tags.length > 0 ? data.tags : [];
    this.bookmark = data && data.bookmark !== 0 ? data.bookmark : '';
    this.status = data && data.status !== 0 ? data.status : '';
    this.progress = data && data.progress !== 0 ? data.progress : 0 ; 
    this.duration = data && data.duration !== 0 ? data.duration : 0 ; 
  }

  get SymbolIcon() {
    switch (this.type) {
      case 'blog':
        return 'notes';
      case 'video':
        return 'video_library';
      case 'pdf':
        return 'picture_as_pdf';
      default:
        return 'Error Occured!';
    }
  }

  // get bookmarkIcon() {
  //   if (this.bookmark) return 'star';
  //   else;
  //   return 'star_border';
  // }

  get contentAction() {
    switch (this.type) {
      case 'blog':
        return 'READ';

      case 'video':
        return 'WATCH';

      case 'pdf':
        return ' VIEW';

      default:
        return 'Error Occured!';
    }
  }

  get colorIcon() {
    switch (this.type) {
      case 'blog':
        return '#150E56';

      case 'video':
        return 'red';

      case 'pdf':
        return '#726A95';

      default:
        return 'black';
    }
  }
}
