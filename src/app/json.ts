export const Data = [
  {
    id: 1,
    type: 'blog',
    title: 'Blonde Ambition',
    description:
      'non interdum in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices',
    source:
      'https://webeden.co.uk/mattis/egestas/metus/aenean/fermentum.jsp?proin=orci&at=luctus&turpis=et&a=ultrices&pede=posuere&posuere=cubilia&nonummy=curae&integer=donec&non=pharetra&velit=magna',
    thumbnail: '../assets/demo2.jpg',
    content:
      'in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id',
    category: 'fault-tolerant',
    tags: ["metus" ,"vitae", "ipsum","sajid"],
    bookmark: false,
    status:'unviewed',
      progress:0,
      duration:3
  },
  {
    id: 2,
    type: 'pdf',
    title: 'Running With Scissors',
    description:
      'arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque',
    source:
      'https://deviantart.com/nisl.json?in=habitasse&hac=platea&habitasse=dictumst&platea=maecenas&dictumst=ut&morbi=massa&vestibulum=quis&velit=augue&id=luctus&pretium=tincidunt&iaculis=nulla&diam=mollis&erat=molestie&fermentum=lorem&justo=quisque&nec=ut&condimentum=erat&neque=curabitur&sapien=gravida&placerat=nisi&ante=at&nulla=nibh&justo=in&aliquam=hac&quis=habitasse&turpis=platea&eget=dictumst&elit=aliquam&sodales=augue&scelerisque=quam&mauris=sollicitudin&sit=vitae&amet=consectetuer&eros=eget&suspendisse=rutrum&accumsan=at&tortor=lorem&quis=integer&turpis=tincidunt&sed=ante&ante=vel&vivamus=ipsum&tortor=praesent&duis=blandit&mattis=lacinia&egestas=erat&metus=vestibulum&aenean=sed&fermentum=magna&donec=at&ut=nunc&mauris=commodo&eget=placerat&massa=praesent&tempor=blandit&convallis=nam&nulla=nulla&neque=integer&libero=pede&convallis=justo&eget=lacinia&eleifend=eget&luctus=tincidunt&ultricies=eget&eu=tempus&nibh=vel&quisque=pede&id=morbi&justo=porttitor&sit=lorem&amet=id&sapien=ligula&dignissim=suspendisse&vestibulum=ornare&vestibulum=consequat',
    thumbnail: '../assets/demo.png',
    content: 'ante ipsum primis in faucibus orci luctus et ultrices posuere',
    category: 'Balanced',
    tags: ["rhoncus", "aliquet", "pulvinar" ,"sed" ],
    bookmark: false,
    status: 'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 3,
    type: 'video',
    title: 'Fuel',
    description: 'ligula nec sem duis aliquam convallis nunc proin at turpis a',
    source:
      'http://zdnet.com/venenatis/tristique/fusce.html?pellentesque=luctus&ultrices=ultricies&mattis=eu',
    thumbnail: '../assets/demo3.jpg',
    content:
      'euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a',
    category: 'methodology',
    tags: ["lacinia" ,"eget" ,"tincidunt", "eget"],
    bookmark: false,
    status: 'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 4,
    type: 'blog',
    title: 'Legacy, The',
    description:
      'hac habitasse is molestie lorem quisque ut erat curabitur gravida',
    source:
      'https://psu.edu/eget/congue/eget/semper/rutrum/nulla.js?magna=nam&vulputate=dui&luctus=proin&cum=leo&sociis=odio&natoque=porttitor&penatibus=id&et=consequat&magnis=in&dis=consequat&parturient=ut&montes=nulla&nascetur=sed&ridiculus=accumsan&mus=felis&vivamus=ut&vestibulum=at&sagittis=dolor&sapien=quis',
    thumbnail: '../assets/demo2.jpg',
    content:
      'tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed',
    category: 'uniform',
    tags: ["feugiat",  "eros" ,"vestibulum" ,  "lacinia",  "venenatis" ],
    bookmark: false,
    status: 'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 5,
    type: 'pdf',
    title: 'Gorgeous (Boh lee chun)',
    description:
      'leo odio condimentum id lsque volutpat dui maecenas tristique',
    source:
      'http://latimes.com/bibendum/felis/sed/interdum/venenatis.html?sapien=amet&urna=diam&pretium=in&nisl=magna&ut=bibendum&volutpat=imperdiet&sapien=nullam&arcu=orci&sed=pede&augue=venenatis&aliquam=non&erat=sodales&volutpat=sed&in=tincidunt&congue=eu&etiam=felis&justo=fusce&etiam=posuere&pretium=felis&iaculis=sed&justo=lacus&in=morbi&hac=sem&habitasse=mauris&platea=laoreet&dictumst=ut&etiam=rhoncus&faucibus=aliquet&cursus=pulvinar&urna=sed&ut=nisl&tellus=nunc&nulla=rhoncus&ut=dui&erat=vel&id=sem&mauris=sed&vulputate=sagittis&elementum=nam&nullam=congue&varius=risus&nulla=semper&facilisi=porta&cras=volutpat',
    thumbnail: '../assets/demo.png',
    content:
      'magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer',
    category: 'context-sensitive',
    tags: ["eu", "sapien", "cursus" ,"vestibulum"],
    bookmark: false,
     status: 'unviewed',
     progress:0,
     duration:3
  },
  {
    id: 6,
    type: 'video',
    title: 'G.I. Joe: The Movie',
    description:
      'mauris viverra dia potenti nullam porttitor lacus at turpis donec posuere metus vitae',
    source:
      'http://latimes.com/neque/vestibulum/eget/vulputate.aspx?dui=consequat&vel=dui&nisl=nec&duis=nisi&ac=volutpat&nibh=eleifend&fusce=donec&lacus=ut&purus=dolor&aliquet=morbi&at=vel&feugiat=lectus&non=in&pretium=quam&quis=fringilla&lectus=rhoncus&suspendisse=mauris&potenti=enim&in=leo&eleifend=rhoncus&quam=sed&a=vestibulum&odio=sit&in=amet&hac=cursus&habitasse=id&platea=turpis&dictumst=integer&maecenas=aliquet&ut=massa&massa=id&quis=lobortis&augue=convallis&luctus=tortor&tincidunt=risus&nulla=dapibus&mollis=augue&molestie=vel&lorem=accumsan&quisque=tellus&ut=nisi&erat=eu&curabitur=orci&gravida=mauris&nisi=lacinia&at=sapien&nibh=quis&in=libero&hac=nullam&habitasse=sit&platea=amet&dictumst=turpis&aliquam=elementum&augue=ligula&quam=vehicula&sollicitudin=consequat&vitae=morbi&consectetuer=a&eget=ipsum&rutrum=integer&at=a&lorem=nibh&integer=in&tincidunt=quis&ante=justo&vel=maecenas&ipsum=rhoncus&praesent=aliquam&blandit=lacus&lacinia=morbi&erat=quis&vestibulum=tortor&sed=id',
    thumbnail: '../assets/demo3.jpg',
    content:
      'odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus',
    category: 'analyzer',
    tags: ["libero" ,"quis" ,"orci" ,"nullam", "molestie" ] ,
    bookmark: false,
    status:'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 7,
    type: 'blog',
    title: 'Guter Junge',
    description:
      'nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac',
    source:
      'http://ycombinator.com/suspendisse/potenti.xml?amet=sed&lobortis=ante&sapien=vivamus&sapien=tortor&non=duis&mi=mattis&integer=egestas&ac=metus&neque=aenean&duis=fermentum&bibendum=donec&morbi=ut&non=mauris&quam=eget&nec=massa',
    thumbnail: '../assets/demo.png',
    content:
      'semper interdum mauris ullamcorper purus sit amet nulla quisque arcu libero rutrum ac lobortis vel',
    category: 'Implemented',
    tags: ["nulla", "mollis", "molestie", "lorem" ,"quisque"],
    bookmark: false,
    status:'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 8,
    type: 'pdf',
    title: "National Lampoon's Lady Killers ",
    description:
      'fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet',
    source:
      'https://twitter.com/duis/bibendum/morbi.json?molestie=pellentesque&nibh=viverra&in=pede&lectus=ac&pellentesque=diam&at=cras&nulla=pellentesque&suspendisse=volutpat&potenti=dui&cras=maecenas&in=tristique&purus=est&eu=et&magna=tempus&vulputate=semper&luctus=est&cum=quam&sociis=pharetra&natoque=magna&penatibus=ac&et=consequat&magnis=metus&dis=sapien&parturient=ut&montes=nunc&nascetur=vestibulum&ridiculus=ante&mus=ipsum&vivamus=primis&vestibulum=in&sagittis=faucibus&sapien=orci&cum=luctus&sociis=et&natoque=ultrices&penatibus=posuere&et=cubilia&magnis=curae&dis=mauris&parturient=viverra&montes=diam&nascetur=vitae&ridiculus=quam&mus=suspendisse&etiam=potenti&vel=nullam&augue=porttitor&vestibulum=lacus&rutrum=at&rutrum=turpis&neque=donec&aenean=posuere&auctor=metus&gravida=vitae&sem=ipsum&praesent=aliquam&id=non&massa=mauris&id=morbi&nisl=non&venenatis=lectus&lacinia=aliquam&aenean=sit&sit=amet&amet=diam&justo=in&morbi=magna&ut=bibendum&odio=imperdiet&cras=nullam&mi=orci&pede=pede&malesuada=venenatis&in=non&imperdiet=sodales&et=sed&commodo=tincidunt&vulputate=eu&justo=felis&in=fusce&blandit=posuere&ultrices=felis&enim=sed&lorem=lacus&ipsum=morbi',
    thumbnail: '../assets/demo3.jpg',
    content:
      'aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque',
    category: 'neutral',
    tags: ["orci", "luctus",  "ultrices", "posuere", "cubilia"] ,
    bookmark: false,
    status: 'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 9,
    type: 'video',
    title: 'And While We Were Here',
    description:
      'donec ut mauris eget massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies',
    source:
      'https://google.cn/rhoncus/aliquet/pulvinar/sed/nisl/nunc/rhoncus.xml?ipsum=in&praesent=faucibus&blandit=orci&lacinia=luctus&erat=et&vestibulum=ultrices&sed=posuere&magna=cubilia&at=curae&nunc=donec&commodo=pharetra&placerat=magna&praesent=vestibulum&blandit=aliquet&nam=ultrices&nulla=erat&integer=tortor&pede=sollicitudin&justo=mi&lacinia=sit&eget=amet&tincidunt=lobortis&eget=sapien&tempus=sapien&vel=non&pede=mi&morbi=integer&porttitor=ac&lorem=neque&id=duis&ligula=bibendum&suspendisse=morbi&ornare=non&consequat=quam&lectus=nec&in=dui&est=luctus&risus=rutrum&auctor=nulla&sed=tellus&tristique=in&in=sagittis&tempus=dui&sit=vel&amet=nisl&sem=duis&fusce=ac&consequat=nibh&nulla=fusce&nisl=lacus&nunc=purus&nisl=aliquet&duis=at&bibendum=feugiat&felis=non&sed=pretium&interdum=quis&venenatis=lectus&turpis=suspendisse&enim=potenti&blandit=in&mi=eleifend&in=quam&porttitor=a&pede=odio&justo=in&eu=hac&massa=habitasse&donec=platea&dapibus=dictumst&duis=maecenas&at=ut&velit=massa&eu=quis&est=augue&congue=luctus&elementum=tincidunt&in=nulla&hac=mollis&habitasse=molestie&platea=lorem&dictumst=quisque&morbi=ut&vestibulum=erat&velit=curabitur&id=gravida&pretium=nisi&iaculis=at&diam=nibh&erat=in&fermentum=hac&justo=habitasse&nec=platea&condimentum=dictumst&neque=aliquam&sapien=augue&placerat=quam&ante=sollicitudin&nulla=vitae',
    thumbnail: '../assets/demo2.jpg',
    content:
      'ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
    category: 'Advanced',
    tags: ["lacus" , "velit", "vivamus", "vel" ,"nulla" ] ,
    bookmark: false,
    status:'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 10,
    type: 'blog',
    title: 'Blackboards (Takhté Siah)',
    description:
      'vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod',
    source:
      'http://marketwatch.com/mattis/odio/donec/vitae.jsp?risus=orci&semper=pede&porta=venenatis&volutpat=non&quam=sodales&pede=sed',
    thumbnail: '../assets/demo3.jpg',
    content:
      'ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in',
    category: 'infrastructure',
    tags: ["ligula" , "lacus", "curabitur" , "ipsum"  ,"tellus", "semper"],
    bookmark: false,
    status: 'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 11,
    type: 'pdf',
    title: 'The Importance of Being Earnest',
    description:
      'hac habitasse platea dictumrutrum at lorem integer tincidunt ante vel ipsum praesent',
    source:
      'https://comsenz.com/id/massa/id/nisl/venenatis.png?id=eros&ornare=suspendisse&imperdiet=accumsan&sapien=tortor&urna=quis&pretium=turpis&nisl=sed&ut=ante&volutpat=vivamus&sapien=tortor&arcu=duis&sed=mattis&augue=egestas&aliquam=metus&erat=aenean&volutpat=fermentum&in=donec&congue=ut&etiam=mauris&justo=eget&etiam=massa&pretium=tempor&iaculis=convallis&justo=nulla&in=neque&hac=libero&habitasse=convallis&platea=eget&dictumst=eleifend&etiam=luctus&faucibus=ultricies&cursus=eu&urna=nibh&ut=quisque&tellus=id&nulla=justo&ut=sit&erat=amet&id=sapien&mauris=dignissim&vulputate=vestibulum&elementum=vestibulum&nullam=ante&varius=ipsum&nulla=primis&facilisi=in&cras=faucibus&non=orci&velit=luctus&nec=et&nisi=ultrices&vulputate=posuere&nonummy=cubilia&maecenas=curae&tincidunt=nulla&lacus=dapibus&at=dolor&velit=vel&vivamus=est&vel=donec&nulla=odio&eget=justo&eros=sollicitudin&elementum=ut&pellentesque=suscipit&quisque=a&porta=feugiat&volutpat=et&erat=eros&quisque=vestibulum&erat=ac&eros=est&viverra=lacinia&eget=nisi&congue=venenatis&eget=tristique&semper=fusce&rutrum=congue&nulla=diam',
    thumbnail: '../assets/demo2.jpg',
    content:
      'justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est',
    category: 'clear-thinking',
    tags: ["rhoncus", "aliquet" ,"pulvinar", "sed"],
    bookmark: false,
    status:'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 12,
    type: 'video',
    title: 'Abendland',
    description:
      'cubilia curae donec ph tortor sollicitudin mi sit amet lobortis sapien sapien non',
    source:
      'https://foxnews.com/libero/nam.js?pede=rhoncus&lobortis=aliquet&ligula=pulvinar&sit=sed&amet=nisl&eleifend=nunc&pede=rhoncus&libero=dui&quis=vel&orci=sem&nullam=sed&molestie=sagittis&nibh=nam&in=congue&lectus=risus&pellentesque=semper&at=porta&nulla=volutpat&suspendisse=quam&potenti=pede&cras=lobortis&in=ligula&purus=sit&eu=amet&magna=eleifend',
    thumbnail: '../assets/demo3.jpg',
    content:
      'at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum',
    category: 'Horizontal',
    tags: ["faucibus" ,"orci", "luctus",  "ultrices" ],
    bookmark: false,
    status: 'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 13,
    type: 'blog',
    title: 'Boat That Rocked, The ',
    description:
      'ultrices phasellus i arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean',
    source:
      'https://webnode.com/ac.xml?pulvinar=vestibulum&lobortis=aliquet&est=ultrices&phasellus=erat&sit=tortor&amet=sollicitudin&erat=mi&nulla=sit&tempus=amet&vivamus=lobortis&in=sapien&felis=sapien&eu=non&sapien=mi&cursus=integer&vestibulum=ac&proin=neque&eu=duis&mi=bibendum&nulla=morbi&ac=non&enim=quam&in=nec&tempor=dui&turpis=luctus&nec=rutrum&euismod=nulla&scelerisque=tellus&quam=in&turpis=sagittis&adipiscing=dui&lorem=vel&vitae=nisl&mattis=duis&nibh=ac&ligula=nibh&nec=fusce&sem=lacus&duis=purus&aliquam=aliquet&convallis=at&nunc=feugiat&proin=non&at=pretium&turpis=quis&a=lectus&pede=suspendisse&posuere=potenti&nonummy=in&integer=eleifend&non=quam&velit=a&donec=odio&diam=in',
    thumbnail: '../assets/demo.png',
    content: 'enim sit amet nunc viverra dapibus nulla suscipit ligula in',
    category: 'Profit-focused',
    tags: ["eros", "elementum" ,"pellentesque" ,"quisque"],
    bookmark: false,
    status:'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 14,
    type: 'pdf',
    title: 'Beauty in Trouble ',
    description:
      'sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec ut',
    source:
      'https://deviantart.com/sed.jpg?et=amet&commodo=turpis&vulputate=elementum&justo=ligula&in=vehicula&blandit=consequat&ultrices=morbi&enim=a&lorem=ipsum&ipsum=integer&dolor=a&sit=nibh&amet=in&consectetuer=quis&adipiscing=justo&elit=maecenas&proin=rhoncus&interdum=aliquam&mauris=lacus&non=morbi&ligula=quis&pellentesque=tortor&ultrices=id&phasellus=nulla&id=ultrices&sapien=aliquet&in=maecenas&sapien=leo&iaculis=odio&congue=condimentum&vivamus=id&metus=luctus',
    thumbnail: '../assets/demo3.jpg',
    content:
      'in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum',
    category: 'actuating',
    tags: ["magnis", "dis" ,"parturient", "montes", "nascetur"],
    bookmark: false,
    status: 'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 15,
    type: 'video',
    title: 'My Best Enemy ',
    description:
      'convallis duis consequat dui n morbi vel lectus in quam fringilla rhoncus mauris',
    source:
      'http://stanford.edu/at/feugiat/non/pretium/quis.jsp?enim=amet&leo=eleifend&rhoncus=pede&sed=libero&vestibulum=quis&sit=orci&amet=nullam&cursus=molestie&id=nibh&turpis=in&integer=lectus&aliquet=pellentesque&massa=at&id=nulla&lobortis=suspendisse&convallis=potenti&tortor=cras&risus=in&dapibus=purus&augue=eu&vel=magna&accumsan=vulputate&tellus=luctus&nisi=cum&eu=sociis&orci=natoque&mauris=penatibus&lacinia=et&sapien=magnis&quis=dis&libero=parturient&nullam=montes&sit=nascetur&amet=ridiculus&turpis=mus&elementum=vivamus&ligula=vestibulum',
    thumbnail:  '../assets/demo2.jpg',
    content:
      'est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac',
    category: 'Stand-alone',
    tags: ["accumsan" ,"felis" , "dolor" ,"quis","consequat" ],
    bookmark: false,
    status: 'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 16,
    type: 'blog',
    title: 'Massacre Canyon',
    description: 'sed accumsan felis ut at dolor quis odio consequat varius',
    source:
      'https://cornell.edu/tortor/duis/mattis/egestas/metus.aspx?a=maecenas&nibh=tincidunt&in=lacus&quis=at&justo=velit&maecenas=vivamus&rhoncus=vel&aliquam=nulla&lacus=eget&morbi=eros&quis=elementum&tortor=pellentesque&id=quisque&nulla=porta&ultrices=volutpat&aliquet=erat&maecenas=quisque&leo=erat&odio=eros&condimentum=viverra&id=eget&luctus=congue',
    thumbnail: '../assets/demo3.jpg',
    content:
      'eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut',
    category: 'incremental',
    tags: ["velit", "vivamus"  ,"nulla" ,"eget" ,"elementum"],
    bookmark: false,
    status: 'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 17,
    type: 'pdf',
    title: 'Dolls',
    description:
      'ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse',
    source:
      'https://google.com.br/sollicitudin/ut/suscipit/a/feugiat/et.aspx?potenti=placerat&cras=praesent&in=blandit&purus=nam&eu=nulla&magna=integer&vulputate=pede&luctus=justo&cum=lacinia&sociis=eget&natoque=tincidunt&penatibus=eget&et=tempus&magnis=vel&dis=pede&parturient=morbi&montes=porttitor&nascetur=lorem&ridiculus=id&mus=ligula&vivamus=suspendisse&vestibulum=ornare&sagittis=consequat&sapien=lectus&cum=in&sociis=est&natoque=risus&penatibus=auctor&et=sed&magnis=tristique&dis=in&parturient=tempus&montes=sit&nascetur=amet&ridiculus=sem&mus=fusce&etiam=consequat&vel=nulla&augue=nisl&vestibulum=nunc&rutrum=nisl&rutrum=duis&neque=bibendum&aenean=felis&auctor=sed&gravida=interdum&sem=venenatis&praesent=turpis&id=enim&massa=blandit&id=mi&nisl=in&venenatis=porttitor&lacinia=pede&aenean=justo&sit=eu&amet=massa',
    thumbnail: '../assets/demo.png',
    content:
      'vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo',
    category: 'service-desk',
    tags: ["condimentum" ,"neque", "sapien", "placerat"],
    bookmark: false,
    status: 'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 18,
    type: 'video',
    title: 'A Gun for Jennifer',
    description:
      'vel augue vestibulum anteorci luctus et ultrices posuere cubilia curae donec pharetra',
    source:
      'http://house.gov/mi/pede.jpg?odio=nulla&elementum=justo&eu=aliquam&interdum=quis&eu=turpis&tincidunt=eget&in=elit&leo=sodales&maecenas=scelerisque&pulvinar=mauris&lobortis=sit&est=amet&phasellus=eros&sit=suspendisse&amet=accumsan&erat=tortor&nulla=quis&tempus=turpis&vivamus=sed&in=ante&felis=vivamus&eu=tortor&sapien=duis&cursus=mattis&vestibulum=egestas&proin=metus&eu=aenean&mi=fermentum&nulla=donec&ac=ut&enim=mauris&in=eget&tempor=massa&turpis=tempor&nec=convallis&euismod=nulla&scelerisque=neque&quam=libero&turpis=convallis&adipiscing=eget&lorem=eleifend&vitae=luctus&mattis=ultricies&nibh=eu&ligula=nibh&nec=quisque&sem=id&duis=justo&aliquam=sit&convallis=amet&nunc=sapien&proin=dignissim&at=vestibulum&turpis=vestibulum&a=ante&pede=ipsum&posuere=primis&nonummy=in&integer=faucibus&non=orci&velit=luctus&donec=et&diam=ultrices&neque=posuere&vestibulum=cubilia&eget=curae&vulputate=nulla&ut=dapibus&ultrices=dolor&vel=vel&augue=est&vestibulum=donec&ante=odio&ipsum=justo&primis=sollicitudin&in=ut&faucibus=suscipit',
    thumbnail:
    '../assets/demo2.jpg',
    content:
      'vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet',
    category: 'info-mediaries',
    tags: ["ultrices" ,"posuere" ,"cubilia", "curae"] ,
    bookmark: false,
    status:'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 19,
    type: 'blog',
    title: 'Lady of Chance, A',
    description:
      'molestie sed justo pellenteellentesque volutpat dui maecenas tristique est et',
    source:
      'https://ca.gov/orci/mauris/lacinia.html?id=gravida&justo=nisi&sit=at&amet=nibh&sapien=in&dignissim=hac&vestibulum=habitasse&vestibulum=platea&ante=dictumst&ipsum=aliquam&primis=augue&in=quam&faucibus=sollicitudin&orci=vitae&luctus=consectetuer&et=eget&ultrices=rutrum&posuere=at&cubilia=lorem&curae=integer&nulla=tincidunt&dapibus=ante&dolor=vel&vel=ipsum&est=praesent&donec=blandit&odio=lacinia&justo=erat&sollicitudin=vestibulum&ut=sed&suscipit=magna&a=at&feugiat=nunc&et=commodo&eros=placerat&vestibulum=praesent&ac=blandit&est=nam&lacinia=nulla&nisi=integer&venenatis=pede&tristique=justo&fusce=lacinia&congue=eget&diam=tincidunt&id=eget&ornare=tempus&imperdiet=vel&sapien=pede&urna=morbi&pretium=porttitor&nisl=lorem&ut=id&volutpat=ligula&sapien=suspendisse&arcu=ornare&sed=consequat&augue=lectus&aliquam=in&erat=est&volutpat=risus&in=auctor&congue=sed',
    thumbnail: '../assets/demo3.jpg',
    content:
      'ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros',
    category: 'systemic',
    tags: ["pulvinar", "sed" ,"nisl" ,"nunc" ],
    bookmark: false,
    status: 'unviewed',
    progress:0,
    duration:3
  },
  {
    id: 20,
    type: 'pdf',
    title: 'Flight of the Conchords',
    description:
      'vel nisl duis ac nibh fusce lacus purus aliquet at feugiat non pretium quis',
    source:
      'http://spotify.com/nam/nulla.jpg?rhoncus=dolor&sed=sit&vestibulum=amet&sit=consectetuer&amet=adipiscing&cursus=elit&id=proin&turpis=risus&integer=praesent&aliquet=lectus&massa=vestibulum&id=quam&lobortis=sapien&convallis=varius&tortor=ut&risus=blandit&dapibus=non&augue=interdum&vel=in&accumsan=ante&tellus=vestibulum&nisi=ante&eu=ipsum&orci=primis&mauris=in&lacinia=faucibus&sapien=orci&quis=luctus&libero=et&nullam=ultrices&sit=posuere&amet=cubilia&turpis=curae&elementum=duis&ligula=faucibus&vehicula=accumsan&consequat=odio&morbi=curabitur&a=convallis&ipsum=duis&integer=consequat&a=dui&nibh=nec&in=nisi&quis=volutpat&justo=eleifend&maecenas=donec&rhoncus=ut&aliquam=dolor&lacus=morbi&morbi=vel&quis=lectus&tortor=in&id=quam&nulla=fringilla&ultrices=rhoncus&aliquet=mauris&maecenas=enim&leo=leo&odio=rhoncus&condimentum=sed&id=vestibulum&luctus=sit&nec=amet&molestie=cursus&sed=id&justo=turpis&pellentesque=integer&viverra=aliquet&pede=massa&ac=id&diam=lobortis&cras=convallis&pellentesque=tortor&volutpat=risus&dui=dapibus&maecenas=augue&tristique=vel&est=accumsan&et=tellus&tempus=nisi&semper=eu&est=orci&quam=mauris&pharetra=lacinia&magna=sapien',
    thumbnail:'../assets/demo2.jpg',
    content:
      'amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus',
    category: 'fault-tolerant',
    tags: ["pulvinar", "sed" ,"nisl" ,"nunc" ],
    bookmark: false,
    status:
      'unviewed',
      progress:0,
      duration:3
  },
];
