import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogBoxComponent } from './components/dialog-box/dialog-box.component';
import { Data } from './json';
import { Modal } from './modal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'poc';
  DataJson = Data;
  newData:any;

  inProgressArray = <any>[];
  viewedArray = <any>[];
  unviewedArray = <any>[];
  bookmarkArray = <any>[];
  tagsArray = <any>[];
  selectedChipArray = <any>[];

  isMarked: any;

  

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {
    this.DataJson = this.DataJson.map((item) => new Modal(item));

    this.newData = this.DataJson;

    this.filterData( this.newData);
    this.DataJson.forEach((element: any) => {
      element.tags.forEach((element: any) => {
        this.tagsArray.push(element);
      });
    });
  }

  getType(value: any) {
    let dialogRef = this.dialog.open(DialogBoxComponent, {
      data: { cardInfo: value },
    });

    dialogRef.afterClosed().subscribe((result) => {
      // console.log(result);

      result.status = result.progress > 90 ? 'viewed' : 'inProgress';
      this.filterData( this.DataJson );

      // this.DataJson.forEach((element, index) => {
      //   if (element.id == value.id) {
      //     if (this.inProgressArray.indexOf(element.id) == -1) {
      //       this.inProgressArray.push(value);
      //       this.DataJson.splice(index, 1);
  //         }
  //       }
  //     });
    });
  }

  unMark() {
    this.isMarked = !this.isMarked;
  }

  filterData( arr:Array<any> , arr2:Array<any>=[]){
    let filteredData: { id: number; type: string; title: string; description: string; source: string; thumbnail: string; content: string; category: string; tags: string[]; bookmark: boolean; status: string; progress: number; duration: number; }[] = [] 
 
    if(arr2.length === 0){ 
      console.log('if Block' );
      this.unviewedArray = arr.filter ( data => data.status === "unviewed");
      this.viewedArray = arr.filter ( data => data.status === "viewed");
      this.inProgressArray = arr.filter ( data => data.status === "inProgress");
     }
     else{
      console.log('else Block' );
        this.selectedChipArray.forEach((item: any) => { 
          this.DataJson.forEach(element => {
        //  console.log( element.tags.includes(item));
         if( element.tags.includes(item)  ){
          filteredData.push(element);
         }
          });
          
        });

        this.filterData(filteredData);

        // this.unviewedArray = filteredData.filter ( data => data.status === "unviewed");
        // this.viewedArray = filteredData.filter ( data => data.status === "viewed");
        // this.inProgressArray = filteredData.filter ( data => data.status === "inProgress");

     } 

  }


  bookMarkedItemDisplay(){
    // this.bookmarkArray = this.DataJson.filter((data) => data.bookmark);
    // console.log(this.bookmarkArray);

// this.isMarked = !this.isMarked;
const bookmarkArray = [ ...this.inProgressArray.filter((data:any) => data.bookmark),
  ...this.unviewedArray.filter((data:any) => data.bookmark),
  ...this.viewedArray.filter((data:any) => data.bookmark)
 ];
this.isMarked ? this.filterData( bookmarkArray ) : this.filterData(this.DataJson);
    
  }


  
  chipItemDisplay(){
// this.isMarked = !this.isMarked;
// const myArray = [ ...this.inProgressArray.filter((data:any) => this.tagsArray.tagItem),
//   ...this.unviewedArray.filter((data:any) => this.tagsArray.tagItem),
//   ...this.viewedArray.filter((data:any) =>this.tagsArray.tagItem)
//  ];
// this.isMarked ? this.filterData( myArray ) : this.filterData(this.DataJson);
    
// console.log(this.myArray);

  }

  toggleSelection( chip:any ,tagItem:any) {
    chip.toggleSelected();
    console.log(tagItem);
    this.selectedChipArray.push( tagItem );
    this.filterData( this.newData , this.selectedChipArray );

 }
}
