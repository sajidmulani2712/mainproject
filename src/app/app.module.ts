import { NgModule ,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BlogCardComponent } from './components/blog-card/blog-card.component';
import { ImageContainerComponent } from './components/image-container/image-container.component';
import { TitleComponent } from './components/title/title.component';
import { DescriptionComponent } from './components/description/description.component';
import { FooterComponent } from './components/footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';

import {MatCardModule} from '@angular/material/card';
import { DialogBoxComponent } from './components/dialog-box/dialog-box.component';

import { MatDialogModule} from '@angular/material/dialog'
import { PdfComponent } from './components/dialog-box/pdf/pdf.component';
import { VideoComponent } from './components/dialog-box/video/video.component';
import { BlogComponent } from './components/dialog-box/blog/blog.component';
import { NoDataAvailableComponent } from './no-data-available/no-data-available.component';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatChipsModule} from '@angular/material/chips';
import {MatProgressBarModule} from '@angular/material/progress-bar';




@NgModule({
  declarations: [
    AppComponent,
    BlogCardComponent,
    ImageContainerComponent,
    TitleComponent,
    DescriptionComponent,
    FooterComponent,
    DialogBoxComponent,
    VideoComponent,
    PdfComponent,
    BlogComponent,
    NoDataAvailableComponent,
 
   
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,

    MatCardModule,   //new added
    MatDialogModule,
    IvyCarouselModule,
    MatTooltipModule,
    MatChipsModule,
    MatProgressBarModule,
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
